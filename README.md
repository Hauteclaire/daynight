# Just another wallpaper
Made by [Haute](https://hauteclaire.me).

## What's so special about this?
The background image will change depending on time of day. If the local time is above 6pm, the background will be changed to a night version. If it's anything else, it'll be a day version.

Also, some hiragana greetings centered on the screen that'll also change depending on time of day.

## Downloading and installing
You can find this wallpaper as a [Workshop item](http://steamcommunity.com/sharedfiles/filedetails/?id=941017244).

## TODO
- Local weather
- Outside temperature
- Modify the background image to reflect said conditions.

## Things I'd like to make work despite the limitations with JavaScript and a Chromium container.
- Get device information (temps, R/W, I/O, etc.)
